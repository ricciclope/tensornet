#include "utils.h"
#include <iostream>
#include <fstream>
#include <omp.h>
#include <string>

#include <opencv2/opencv.hpp>

#include "opencv2/core/utility.hpp"
#include <opencv2/core/core.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <opencv2/ml.hpp>
using namespace std;
using namespace cv;
using namespace cv::ml;

//definir dataset - #define
//leer dataset - main
//poner en formato - main
//entrenar

//#define ATD at<double>

//#define elif else if

#define cifar10
//#define lfw

#ifdef cifar10
//bool cifar=true;
void read_batch(string filename, vector<Mat> &vec, Mat &label){
    ifstream file (filename, std::ifstream::binary);
    if (file.is_open())
    {
        int number_of_images = 10000;
        int n_rows = 32;
        int n_cols = 32;
        for(int i = 0; i < number_of_images; ++i)
        {
            char * tplabel=new char [0];
            file.read(tplabel, 1);
            //vector<Mat> channels;
            Mat channels[3];
            Mat fin_img = Mat::zeros(n_rows, n_cols, CV_8UC3);
            for(int ch = 0; ch < 3; ch++){
                Mat tp = Mat::zeros(n_rows, n_cols, CV_8UC1);
                for(int r = 0; r < n_rows; ++r){
                    for(int c = 0; c < n_cols; ++c){
                        unsigned char temp = 0;
                        file.read((char*) &temp, sizeof(temp));
                        tp.at<uchar>(r, c) = (int) temp;
                    }
                }
                //channels.push_back(tp.clone());
                channels[ch]=tp.clone();
            }
            merge(channels,3, fin_img);
            vec.push_back(fin_img.clone());

//                        imshow(to_string(*tplabel),fin_img);
//                        waitKey(0);

            cout<<"sizes"<<fin_img.size<<"sizes"<<fin_img.channels()<<"label"<<(int)*tplabel<<endl;
            label.at<double>(0, i) =(double) *tplabel;
        }
    }
}

Mat concatenateMat(vector<Mat> &vec){

    int height = vec[0].rows;
    int width = vec[0].cols;
    Mat res = Mat::zeros(height * width, vec.size(), CV_64FC1);
    for(int i=0; i<vec.size(); i++){
        Mat img(height, width, CV_64FC1);
        Mat gray(height, width, CV_8UC1);
        cvtColor(vec[i], gray, CV_RGB2GRAY);
        gray.convertTo(img, CV_64FC1);
        // reshape(int cn, int rows=0), cn is num of channels.
        Mat ptmat = img.reshape(0, height * width);
        Rect roi = cv::Rect(i, 0, ptmat.cols, ptmat.rows);
        Mat subView = res(roi);
        ptmat.copyTo(subView);
    }
    divide(res, 255.0, res);
    return res;
}

Mat concatenateMatC(vector<Mat> &vec){

    int height = vec[0].rows;
    int width = vec[0].cols;
    Mat res = Mat::zeros(height * width * 3, vec.size(), CV_64FC1);
    for(int i=0; i<vec.size(); i++){
        Mat img(height, width, CV_64FC3);
        vec[i].convertTo(img, CV_64FC3);
        vector<Mat> chs;
        split(img, chs);
        for(int j = 0; j < 3; j++){
            Mat ptmat = chs[j].reshape(0, height * width);
            Rect roi = cv::Rect(i, j * ptmat.rows, ptmat.cols, ptmat.rows);
            Mat subView = res(roi);
            ptmat.copyTo(subView);
        }
    }
    divide(res, 255.0, res);
    return res;
}

void read_CIFAR10(Mat &trainX, Mat &testX, Mat &trainY, Mat &testY){

    string filename;
    filename = "cifar-10-batches-bin/data_batch_1.bin";
    vector<Mat> batch1;
    Mat label1 = Mat::zeros(1, 10000, CV_64FC1);
    read_batch(filename, batch1, label1);

    filename = "cifar-10-batches-bin/data_batch_2.bin";
    vector<Mat> batch2;
    Mat label2 = Mat::zeros(1, 10000, CV_64FC1);
    read_batch(filename, batch2, label2);

    filename = "cifar-10-batches-bin/data_batch_3.bin";
    vector<Mat> batch3;
    Mat label3 = Mat::zeros(1, 10000, CV_64FC1);
    read_batch(filename, batch3, label3);

    filename = "cifar-10-batches-bin/data_batch_4.bin";
    vector<Mat> batch4;
    Mat label4 = Mat::zeros(1, 10000, CV_64FC1);
    read_batch(filename, batch4, label4);

    filename = "cifar-10-batches-bin/data_batch_5.bin";
    vector<Mat> batch5;
    Mat label5 = Mat::zeros(1, 10000, CV_64FC1);
    read_batch(filename, batch5, label5);

    filename = "cifar-10-batches-bin/test_batch.bin";
    vector<Mat> batcht;
    Mat labelt = Mat::zeros(1, 10000, CV_64FC1);
    read_batch(filename, batcht, labelt);

    Mat mt1 = concatenateMat(batch1);
    Mat mt2 = concatenateMat(batch2);
    Mat mt3 = concatenateMat(batch3);
    Mat mt4 = concatenateMat(batch4);
    Mat mt5 = concatenateMat(batch5);
    Mat mtt = concatenateMat(batcht);

    Rect roi = cv::Rect(mt1.cols * 0, 0, mt1.cols, trainX.rows);
    Mat subView = trainX(roi);
    mt1.copyTo(subView);
    roi = cv::Rect(label1.cols * 0, 0, label1.cols, 1);
    subView = trainY(roi);
    label1.copyTo(subView);

    roi = cv::Rect(mt1.cols * 1, 0, mt1.cols, trainX.rows);
    subView = trainX(roi);
    mt2.copyTo(subView);
    roi = cv::Rect(label1.cols * 1, 0, label1.cols, 1);
    subView = trainY(roi);
    label2.copyTo(subView);

    roi = cv::Rect(mt1.cols * 2, 0, mt1.cols, trainX.rows);
    subView = trainX(roi);
    mt3.copyTo(subView);
    roi = cv::Rect(label1.cols * 2, 0, label1.cols, 1);
    subView = trainY(roi);
    label3.copyTo(subView);

    roi = cv::Rect(mt1.cols * 3, 0, mt1.cols, trainX.rows);
    subView = trainX(roi);
    mt4.copyTo(subView);
    roi = cv::Rect(label1.cols * 3, 0, label1.cols, 1);
    subView = trainY(roi);
    label4.copyTo(subView);

    roi = cv::Rect(mt1.cols * 4, 0, mt1.cols, trainX.rows);
    subView = trainX(roi);
    mt5.copyTo(subView);
    roi = cv::Rect(label1.cols * 4, 0, label1.cols, 1);
    subView = trainY(roi);
    label5.copyTo(subView);

    mtt.copyTo(testX);
    labelt.copyTo(testY);

}

#endif

/*
g++ -std=c++11 -o cifar `pkg-config opencv --cflags` readcifar10.cpp `pkg-config opencv --libs`

 ===== Results of PCANet, followed by a linear SVM classifier =====
     PCANet training time: 182.09 secs.
     Linear SVM training time: 41.97 secs.
     Testing error rate: 42.50%
     Average testing time 0.31 secs per test sample.

 * */

/*
5000 samples
Computing PCA filter bank and its outputs at stage 0...
stage0 PCA_FilterBank time: 1.17194
stage0 output time: 1.6431
Computing PCA filter bank and its outputs at stage 1...
stage1 PCA_FilterBank time: 10.2845
stage1 output time: 1.8e-07

totle FilterBank time: 13.1036

hasing time: 13.4685
PCANet Training time: 28.3163

====== SVM Train =======
SVM Train time: 162.129

====== PCANet Testing =======

test time usage: 54.9898
Accuracy: 0.4621
person1 accuracy: 0.646
person2 accuracy: 0.356
person3 accuracy: 0.3
person4 accuracy: 0.397
person5 accuracy: 0.352
person6 accuracy: 0.496
person7 accuracy: 0.469
person8 accuracy: 0.616
person9 accuracy: 0.451
person10 accuracy: 0
test images num for each class: 1000
*/

/*
 * 500samples
Computing PCA filter bank and its outputs at stage 0...
 stage0 PCA_FilterBank time: 0.120393
 stage0 output time: 0.139025
 Computing PCA filter bank and its outputs at stage 1...
 stage1 PCA_FilterBank time: 0.900541
 stage1 output time: 2.18e-07

 totle FilterBank time: 1.16501

 hasing time: 1.31888
 PCANet Training time: 2.73212

 ====== Training Linear SVM Classifier =======


 ====== SVM Train =======
 SVM Train time: 9.01195

 ====== PCANet Testing =======

 test time usage: 58.2871
Accuracy: 0.3992
person1 accuracy: 0.593
person2 accuracy: 0.199
person3 accuracy: 0.232
person4 accuracy: 0.372
person5 accuracy: 0.244
person6 accuracy: 0.528
person7 accuracy: 0.431
person8 accuracy: 0.416
person9 accuracy: 0.478
person10 accuracy: 0
test images num for each class: 1000
 */


int main(int argc, char** argv){

    //*******
    //READ DATASETS

#ifdef cifar10

    Mat trainX, testX;
    Mat trainY, testY;
    trainX = Mat::zeros(1024, 50000, CV_64FC1);
    testX = Mat::zeros(1024, 10000, CV_64FC1);
    trainY = Mat::zeros(1, 50000, CV_64FC1);
    testY = Mat::zeros(1, 10000, CV_64FC1);

    read_CIFAR10(trainX, testX, trainY, testY);

    vector<Mat> InImgs;
    int DIR_NUM=10;//number of classes
    int NUM=trainX.cols/100;//number of training samples
    int labels[NUM];

    //***********************
    for (int ii=0;ii<NUM;ii++) {

        Mat result;
        //cout<<"sizerow"<<trainX.row(0).rows<<trainX.row(0).cols<<endl;
        //cout<<"sizetrainx"<<trainX.size<<"channels"<<trainX.channels()<<endl;
        result=trainX.col(ii).clone();
        result=result.t();
        result=result.reshape(0,32);
        InImgs.push_back(result.clone());
        double aux=trainY.at<double>(0,ii);
        labels[ii]=(int)aux;
        cout<<"labelcifar "<<labels[ii]<<  " "  <<aux<<endl;
    }


    int NUMt=testX.cols;
    vector<Mat> testImg;
    int testLabel[NUMt];

    //***********************
    for (int ii=0;ii<NUMt;ii++) {

        Mat result;
        //cout<<"sizerow"<<trainX.row(0).rows<<trainX.row(0).cols<<endl;
        //cout<<"sizetrainx"<<trainX.size<<"channels"<<trainX.channels()<<endl;
        result=testX.col(ii).clone();
        result=result.t();
        result=result.reshape(0,32);
        testImg.push_back(result.clone());

        double aux=testY.at<double>(0,ii);
        testLabel[ii]=int(aux);
        cout<<"labelcifar"<<testLabel[ii]<<aux<<endl;
    }

#endif

#ifdef lfw
    const int DIR_LENGTH = 256;
    const int DIR_NUM = 7;
    //input image size height: 60, width: 48
    // 路径根据自己的情况来修改即可

    const char *dir[DIR_NUM] = {
        "../datas/train/1/1_",
        "../datas/train/2/2_",
        "../datas/train/3/3_",
        "../datas/train/4/4_",
        "../datas/train/5/5_",
        "../datas/train/6/6_",
        "../datas/train/7/7_"
    };

    const char *test_dir[DIR_NUM] = {
        "../datas/test/1/1_",
        "../datas/test/2/2_",
        "../datas/test/3/3_",
        "../datas/test/4/4_",
        "../datas/test/5/5_",
        "../datas/test/6/6_",
        "../datas/test/7/7_"
    };

    char path[DIR_LENGTH];
    Mat img;
    Mat change;
    vector<Mat> InImgs;

    const int train_num = 40;
    const int NUM = DIR_NUM * train_num;

    int labels[NUM];// = new float();
    int x = 0;
    for(int i=1; i<train_num + 1; i++){
        for(int j=1; j<=DIR_NUM; j++){
            sprintf(path, "%s%d%s", dir[j-1], i, ".jpg");
            //cout<<"imagepath"<< path<<endl;
            img = imread(path,0);
            //imshow("ric",img);
            //waitKey(1000);

            img.convertTo(change,CV_64FC1,1.0/255);
            //imshow("ric1",change);
            //waitKey(1000);

            InImgs.push_back(change.clone());
            //cv::imshow("input1", InImgs[i+j-2]);
            //cv::waitKey(100);
            labels[x] = j;
            x++;
        }
    }

#endif


    //**********************
    //PCANET PARAMS
    vector<int> NumFilters;
    NumFilters.push_back(8);//8
    NumFilters.push_back(8);//8
    vector<int> blockSize;
    blockSize.push_back(12);   //  height / 4
    blockSize.push_back(10);    //  width / 4


    PCANet pcaNet = {
        2,
        7,
        NumFilters,
        blockSize,
        0.5
    };

    //********************************

    cout <<"\n ====== PCANet Training ======= \n"<<endl;
    int64 e1 = cv::getTickCount();

    cout<<"ricisawesome"<<endl;

    //    for(int ii=0;ii<InImgs.size();ii++){
    //        cv::imshow("input", InImgs[ii]);
    //        cv::waitKey(100);
    //    }

    auto InImgs1=InImgs;
    PCA_Train_Result* result = PCANet_train(InImgs, &pcaNet, true);
    int64 e2 = cv::getTickCount();
    double time = (e2 - e1)/ cv::getTickFrequency();
    cout <<" PCANet Training time: "<<time<<endl;

    ///*******************************************

    //	FileStorage fs("../model/filters.xml", FileStorage::WRITE);
    //	fs<<"filter1"<<result->Filters[0]<<"filter2"<<result->Filters[1];
    //	fs.release();


    ///*********************************************************

    //  svm create dataset  //////////
    cout <<"\n ====== Training Linear SVM Classifier ======= \n"<<endl;

    int new_labels[NUM];// = new float[NUM]();//total number of samples
    int size = result->feature_idx.size();//280


    for(int i=0; i<size; i++){
        new_labels[i] = labels[result->feature_idx[i]];
        //cout<<"labels"<<new_labels[i]<<endl;
    }


    Mat labelsMat(NUM, 1, CV_32S, new_labels);



    //************************
    //*****SORT FEATURES BY CLASS
    //    Mat traindata, matfeat;
    //    traindata=result->Features;
    //    //traindata.convertTo(traindata,CV_8UC1,10);


    //    for (int kk=0;kk<3;kk++) {//numero de clases
    //        for (int ii=0;ii<traindata.rows;ii++) {
    //            if(kk==ii%2){
    //                cv::Mat ricfeatures=traindata.row(ii);
    //                //ricfeatures=ricfeatures(cv::Rect2d(0,0,400,1));//select les features for a sample
    //                //                resize(ricfeatures,ricfeatures,cv::Size(),1,50);
    //                //                cv::imshow("ricfeat",ricfeatures);
    //                //                cv::imshow("input",InImgs1[ii]);
    //                //                cv::waitKey(1000);
    //                matfeat.push_back(traindata.row(ii));
    //            }
    //        }
    //    }


    //ric DRAW all FEATURES

    //    //    traindata=result->Features(Rect2d(0,0,400,80));
    //    traindata=matfeat;
    //    traindata.convertTo(traindata,CV_8UC1,10);
    //    resize(traindata,traindata,Size(),(double)1/100,4);
    //    imshow("train",traindata);
    //    //waitKey();
    //    //end

    //***********************
    //PCA compute and project data to principal vectors
    //    cout<<"cov size"<<matfeat.size<<endl;

    //    PCA pca_analysis(matfeat, Mat(), PCA::DATA_AS_ROW,10);

    //    //pca_analysis.eigenvectors.row(0);

    //    Mat projection=pca_analysis.project(matfeat);

    //    projection.convertTo(projection,CV_8UC1,10);

    //    resize(projection,projection,Size(),40,10,INTER_LINEAR_EXACT );
    //    imshow("projection",projection);
    //    waitKey();


    //****************
    //TRIED TO COMPUTE PCA MANUALLY, THROW MEMORY ERROR

    //    cv::Mat eValuesMat;
    //    cv::Mat eVectorsMat;
    //    Mat Rx=Mat::zeros(matfeat.cols,matfeat.cols , CV_8U);
    //cout<<"allocate"<<endl;
    //    Rx = matfeat.t() * matfeat;
    //    cout<<"mult"<<endl;
    //    Rx = Rx / (double)(InImgs1.size());

    //    eigen(Rx, eValuesMat, eVectorsMat);
    //***********************





    //*******************************

    result->Features.convertTo(result->Features, CV_32F);

    ///************************************************************

    //svm initialization
    Ptr<SVM> svm = SVM::create();
    svm->setType(SVM::C_SVC);
    svm->setC( 1 );
    svm->setKernel(SVM::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));

    e1 = cv::getTickCount();
    svm->train(result->Features, ROW_SAMPLE, labelsMat);//trainAuto
    e2 = cv::getTickCount();
    time = (e2 - e1)/ cv::getTickFrequency();

    cout <<"\n ====== SVM Train ======="<<endl;

    cout <<" SVM Train time: "<<time<<endl;

    // draw svm

    //    // Data for visual representation
    //    int width = 512, height = 512;
    //    Mat image = Mat::zeros(height, width, CV_8UC3);
    //    // Show the decision regions given by the SVM
    //    Vec3b green(0,255,0), blue(255,0,0);
    //    for (int i = 0; i < image.rows; i++)
    //    {
    //        for (int j = 0; j < image.cols; j++)
    //        {
    //            Mat sampleMat = (Mat_<float>(1,2) << j,i);
    //            float response = svm->predict(sampleMat);
    //            if (response == 1)
    //                image.at<Vec3b>(i,j)  = green;
    //            else if (response == -1)
    //                image.at<Vec3b>(i,j)  = blue;
    //        }
    //    }


    //*************************************************************************************

    cout <<"\n ====== PCANet Testing ======= \n"<<endl;
#ifdef LFW
    vector<Mat> testImg;
    vector<int> testLabel;
    vector<string> names;
    string *t;

    int testNum = 24;

    for(int i=41; i<65; i++){
        for(int j=0; j<DIR_NUM; j++){
            sprintf(path, "%s%d%s", test_dir[j], i, ".jpg");
            img = imread(path, 0);
            t = new string(path);
            names.push_back(*t);

            img.convertTo(change,CV_64FC1,1.0/255);

            testImg.push_back(change.clone());
            testLabel.push_back(j + 1);
        }
    }

#endif

    //to init
    //int testSIze = testImg.size();
    int testSIze=testX.cols/10;

    Hashing_Result * hashing_r;
    PCA_Out_Result * out;

    //to init
    //float all = DIR_NUM * testNum;
    int testNum=testSIze/DIR_NUM;
    float all=testSIze;
    float correct = 0;
    int coreNum = omp_get_num_procs();//获得处理器个数

    float corrs[DIR_NUM];//number of corrects per class
    for(int i=0; i<DIR_NUM; i++)
        corrs[i] = 0;

    ///////////************************************************


    e1 = cv::getTickCount();
#pragma omp parallel for default(none) num_threads(coreNum) private(out, hashing_r) shared(corrs, correct, testLabel, svm, pcaNet, testSIze, testImg, result)
    for(int i=0; i<testSIze; i++){
        out = new PCA_Out_Result;
        out->OutImgIdx.push_back(0);
        out->OutImg.push_back(testImg[i].clone());
        out = PCA_output(out->OutImg, out->OutImgIdx, pcaNet.PatchSize,
                         pcaNet.NumFilters[0], result->Filters[0], 2);
        for(int j=1; j<pcaNet.NumFilters[1]; j++)
            out->OutImgIdx.push_back(j);

        out = PCA_output(out->OutImg, out->OutImgIdx, pcaNet.PatchSize,
                         pcaNet.NumFilters[1], result->Filters[1], 2);
        hashing_r = HashingHist(&pcaNet, out->OutImgIdx, out->OutImg);
        hashing_r->Features.convertTo(hashing_r->Features, CV_32F);

        //******************************************************************
        //Mat alpha, svidx;
        float pred = svm->predict(hashing_r->Features);

        //cout<<"pred "<<(int)pred<<" testlabel"<<testLabel[i]<<endl;
        //cout<<"decisionfunct"<<svm->getDecisionFunction(20,alpha, svidx)<<endl;
#pragma omp critical
        {
            if(pred == testLabel[i]){

//                cout<<"pred"<<pred << " vs " << "label "<< testLabel[i]<<"corrsid"<<testLabel[i]-1<<endl;

                corrs[testLabel[i]]++;
                correct ++;
            }
        }
        delete out;
    }

    //***************************************************************************
    e2 = cv::getTickCount();
    time = (e2 - e1)/ cv::getTickFrequency();

    for(int i=0; i<DIR_NUM; i++)
        cout << "person" <<i+1<<" accuracy: "<<corrs[i] / testNum<<endl;
    //cout <<"test images num for each class: "<<testNum<<endl;
    cout<<"number of test samples"<<testSIze<<endl;
    cout<<"per sample inference time"<<time/testSIze<<endl;

    cout <<" test time usage: "<<time<<endl;
    cout <<"Accuracy: "<<correct / all<<endl;
    //*******************************************************************************


    return 0;
}
