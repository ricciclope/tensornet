//main.cpp PCANet_train(InImgs, &pcaNet, true);
//utils.cpp PCANet_train(vector<cv::Mat>& InImg, PCANet* PcaNet, bool is_extract_feature) INPUT FUNCTION
//PCA_FilterBank(out_result->OutImg, PcaNet->PatchSize, PcaNet->NumFilters[s]) COMPUTE FILTERS
//cv::Mat PCA_FilterBank(vector<cv::Mat>& InImg, int PatchSize, int NumFilters) COMPUTE FILTERS
//PCA_output(out_result->OutImg, out_result->OutImgIdx, PcaNet->PatchSize, PcaNet->NumFilters[s], train_result->Filters[s], omp_get_num_procs())
//COMPUTE CONVOLUTION // FILTER IMAGES
//PCA_Out_Result* PCA_output(vector<cv::Mat>& InImg, vector<int>& InImgIdx, int PatchSize, int NumFilters, cv::Mat& Filters, int threadnum)
//COMPUTE CONVOLUTION // FILTER IMAGES
//filter for PCA_output is cv::Mat& Filters


// for each stage
    //filterbank(all input images)
    //For all input images
        //for each image
            //extract all patches from an image
            //vector<Mat> im2matvect(cv::Mat& InImg, vector<int>& blockSize, vector<int>& stepSize){
            //remove mean from each patch
            //temp=temp3*temp3.t() //COVARIANCE
            //Rx=Rx+temp


//vector<Mat> patches;



#include "utils.h"
#include <fstream>
#include <omp.h>

#include <opencv2/opencv.hpp>

#include "opencv2/core/utility.hpp"
#include <opencv2/core/core.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"


#include <eigen3/Eigen/Dense>
#include "opencv2/core/eigen.hpp"

using namespace Eigen;

using namespace cv;
using namespace std;

Mat filtfromcov(vector<MatrixXd> S, Mat src)
{
    //EXTRACT BLOCKS
    //COV TENSOR UNFOLD MODE 1

    MatrixXd S1(src.rows,src.rows*src.cols*src.cols);
    for(int i=0;i<src.cols;i++){//I2
        S1.block(0,i*src.rows*src.cols,src.rows,src.rows*src.cols) = S[0].block(i*src.rows,0,src.rows,src.rows*src.cols);
    }

//    cout<<"S1 "<<S1.rows()<< "  "<< S1.cols()<<endl;

//    #ifdef debug
//    cout<<S1<<endl;
//    #endif

    //COMPUTE U1
    BDCSVD < MatrixXd > svd(S1, Eigen::ComputeThinU | Eigen::ComputeThinV);
    MatrixXd U1;
    U1=svd.matrixU();


    //Multiply covariance tensor times U1^T and U2^T
    //S=G_S x1 U1 x2 U1 x3 U2 x4 U2

    //COMPUTE U1TxS
    MatrixXd U1TxS;

    U1TxS=U1.transpose()*S1;

    #ifdef debug
    cout<<"U1TxS"<<endl<<U1TxS<<endl;
    #endif

    //ARRANGE U1TxS AS A TENSOR A1
    MatrixXd A1(U1.cols()*src.cols,src.rows*src.cols);//tensor of U1TxS

    for(int ii=0; ii<src.cols; ii++){
        A1.block(ii*U1.cols(),0,U1.cols(),src.rows*src.cols)=
        U1TxS.block(0,ii*src.rows*src.cols,U1.cols(),src.rows*src.cols);
    }

    #ifdef debug
    cout<<"A1"<<endl<<A1<<endl;
    #endif

//    Block operation
//    Block of size (p,q), starting at (i,j)
//
//    Version constructing a dynamic-size block expression
//    matrix.block(i,j,p,q);
//
//    Version constructing a fixed-size block expression
//    matrix.block<p,q>(i,j);

    //COMPUTE A1 x3 U2^T

    //UNFOLD A1 in mode 3

    MatrixXd A1_3(src.cols, U1.cols()*src.rows*src.cols);


    for(int i2=0;i2<src.rows*src.cols;i2++){
        for(int i1=0;i1<U1.cols();i1++){//FILL I1=r1 columns
            for(int i3=0;i3<src.cols;i3++){ //FILL FIRST COLUMN
                A1_3(i3,i1+i2*U1.cols())=A1(i3*U1.cols()+i1,i2);
            }
        }
    }

    #ifdef debug
    cout<<"A1_3 "<<endl<<A1_3<<endl;
    #endif

    //***********************************************************
    //MODE 3 FACTOR MATRIX

    //EXTRACT BLOCKS AND UNFOLD INTO MODE 1

    MatrixXd S2(src.cols,src.rows*src.rows*src.cols);

    for(int i=0;i<src.rows;i++){//I1
        S2.block(0,i*src.rows*src.cols,src.cols,src.rows*src.cols)= S[1].block(i*src.cols,0,src.cols,src.rows*src.cols);
    }

//    cout<<"S2 "<<S2.rows()<< "  "<< S2.cols()<<endl;
//    #ifdef debug
//    cout<<S2<<endl;
//    #endif

    //compute U2
    BDCSVD < MatrixXd > svd2(S2, Eigen::ComputeThinU);
    MatrixXd U2;
    U2=svd2.matrixU();
    cout<<"DECOMPOSE S2"<<endl;
    //****************************************************
    //COMPUTE A1 x3 U2^T
    //*****************************************************

    MatrixXd A2;

    A2=U2.transpose()*A1_3;

    #ifdef debug
    cout<< "A2 "<<endl<<A2<<endl;
    #endif

    //**************************
    //EXTRACT IMAGE SPACES
    //**************************
    cout<<"beforeextractspaces"<<endl;
    vector<MatrixXd> spaces(U1.cols()*U2.cols());
    MatrixXd spaceaux(src.rows,src.cols);
    vector<VectorXd> vectoraux(U2.cols()*U1.cols(), VectorXd(src.rows*src.cols));
    MatrixXd joined(src.rows*src.cols,U2.cols()*U1.cols());
    Mat spacecv;
    Mat resulth;
    Mat resultv;

    namedWindow("ric",WINDOW_NORMAL);

    Mat filters;//imagespaces in rows

    for(int i4=0; i4<U2.cols();i4++){
        for(int i3=0;i3<U1.cols();i3++){
            for(int i1=0; i1<src.cols ; i1++){
                for(int i2=0; i2<src.rows; i2++){
                    spaceaux(i2,i1)=A2(i4,i3+i2*U1.cols()+i1*U1.cols()*src.rows);
                }
            }

            #ifdef debug
            cout<< "spaceaux"<<endl<<spaceaux<<endl;
            #endif

            //imagespaces in eigen matrix spaceaux
            //vectorize each imagespace
//            cout<<"idx"<<i3+i4*U1.cols()<<"total"<<U2.cols()*U1.cols()<<endl;
            int idx=i3+i4*U1.cols();
            vectoraux[idx]=Map<VectorXd>(spaceaux.data(), spaceaux.size());
            cout<<"vectoraux[idx].norm();"<<vectoraux[idx].norm()<<endl;
            vectoraux[idx]=vectoraux[idx]/vectoraux[idx].norm();

//            cout<<"termina"<<endl;
//            cout<<"v "<<vectoraux.rows()<<"  vectoraux "<<vectoraux.cols()<<endl;



            eigen2cv(spaceaux,spacecv);

            double min, max;
            cv::minMaxLoc(spacecv, &min, &max);

            spacecv=(spacecv-min)/(max-min)*255;
            spacecv.convertTo(spacecv,0);

            if (resulth.empty()){
                spacecv.copyTo(resulth);
            }
            else{
                hconcat(resulth,spacecv,resulth);
            }

//            imshow("ric",spacecv);
//            waitKey();
        }

        if (resultv.empty()){
            resulth.copyTo(resultv);
        }
        else{
            vconcat(resultv,resulth,resultv);
        }
        resulth=Mat();
    }


    for(int i=0;i<vectoraux.size();i++){
//        cout<<"idx "<<i<<endl;
//        cout<<"joined "<<"rows "<<joined.rows()<<" cols "<<joined.cols()<<endl;
//        cout<<"vectoraux[0] "<<"rows "<<vectoraux[0].rows()<<" cols "<<vectoraux[0].cols()<<endl;
        joined.block(0,i,vectoraux[0].rows(),1) = vectoraux[i];

    }
    resize(resultv,resultv,Size(320,320));
    imshow("ric",resultv);
//    imwrite( "../pcanet/results/filter"+to_string(imageidx)+".bmp", resultv );
//    cout<<"../results/filter"+to_string(imageidx)+".bmp"<<endl;

//    waitKey();

    eigen2cv(joined,filters);

    return filters;

}

vector<MatrixXd> computecovariance(vector<Mat> srcvector)
{

    Mat src= srcvector[0];
    int64 e1,e2;
    double time;

    //CONVERT FROM OCV TO EIGEN
    //vector of mats to eigen
    //tensor from image
    int samples=srcvector.size();
    vector<MatrixXd> eigaux(samples);//(src.rows, src.cols)
    vector<VectorXd> v(samples);

    //UNFOLD MATRICES TO COLUMN VECTORS
    for(int i=0;i<samples;i++){
        cv2eigen(srcvector[i],eigaux[i]);

        //cout<<"bgr"<<endl<<bgr[i]<<endl;

        //Vectorize matrices
        v[i]=Map<VectorXd>(eigaux[i].data(), eigaux[i].size());
//        cout<<endl << "v[i].rows() "<<v[i].rows()<<" v[i].cols()  "<<v[i].cols()<<endl;
        //cout<<v[i]<<endl;
    }


    //ARRANGE VECTORIZED IMAGES TO MATRIX X
    MatrixXd X(v[0].rows(), v.size());

    for(int i=0;i<v.size();i++){
        X.block(0,i,v[0].rows(),1) = v[i];
    }

    cout<<"X "<<X.rows()<< "  "<< X.cols()<<endl;
    //cout<<X<<endl;

    //REMOVE MEAN FROM EACH VARIABLE
    //remove mean of each dimension, dimensions at each row

//    X.colwise() -= X.rowwise().sum()/X.cols(); //remove mean of each dimension
    X.rowwise() -= X.colwise().mean(); // remove mean of each image

//    cout<<"removed mean"<<endl<<X<<endl;

    //COMPUTE COVARIANCE MATRIX

    cout<<"before alloc"<<endl;
    MatrixXd S1(X.rows(),X.rows());
    cout<<"after alloc"<<endl;
    S1=X*X.transpose();



    //******************************************************************
    //Extract rows and build S2
    //**************************************************************
    v=vector<VectorXd>(samples);

    for(int i=0;i<samples;i++){
        cv2eigen(srcvector[i].t(),eigaux[i]);
    //        cout<<"bgr"<<endl<<bgr[i]<<endl;

        v[i]=Map<VectorXd>(eigaux[i].data(), eigaux[i].size());
//        cout<<"v "<<v[i].rows()<<"   "<<v[i].cols()<<endl;
    }

    X=MatrixXd(v[0].rows(), v.size());
    for(int i=0;i<v.size();i++){
        X.block(0,i,v[0].rows(),1) = v[i];
    }

    //    X.colwise() -= X.rowwise().sum()/X.cols(); //remove mean of each dimension
    X.rowwise() -= X.colwise().mean(); // remove mean of each image

    //    cout<<"X "<<X.rows()<< "  "<< X.cols()<<endl;

    //    cout<<"before alloc"<<endl;
    MatrixXd S2(X.rows(),X.rows());
    //    cout<<"after alloc"<<endl;
    S2=X*X.transpose();

    vector<MatrixXd> covariancevector;
    covariancevector.push_back(S1);
    covariancevector.push_back(S2);


    return covariancevector;
}

Mat tensorfilters(vector<Mat> srcvector, int imageidx)
{
//    for(int ii=0;ii<srcvector.size();ii++){
//        Mat aux=srcvector[ii];
//        resize(aux,aux,Size(),4,4);
//        imshow("ric",aux);
//        waitKey();
//    }
    Mat src= srcvector[0];
    int64 e1,e2;
    double time;
 
    //CONVERT FROM OCV TO EIGEN
    //vector of mats to eigen
    //tensor from image
    int samples=srcvector.size();
    vector<MatrixXd> eigaux(samples);//(src.rows, src.cols)
    vector<VectorXd> v(samples);

    //UNFOLD MATRICES TO COLUMN VECTORS
    for(int i=0;i<samples;i++){
        cv2eigen(srcvector[i],eigaux[i]);
        
        //cout<<"bgr"<<endl<<bgr[i]<<endl;
        
        //Vectorize matrices
        v[i]=Map<VectorXd>(eigaux[i].data(), eigaux[i].size());
//        cout<<endl << "v[i].rows() "<<v[i].rows()<<" v[i].cols()  "<<v[i].cols()<<endl;
        //cout<<v[i]<<endl;
    }   
    
    
    //ARRANGE VECTORIZED IMAGES TO MATRIX X
    MatrixXd X(v[0].rows(), v.size());

    for(int i=0;i<v.size();i++){
        X.block(0,i,v[0].rows(),1) = v[i];
    }
    
    cout<<"X "<<X.rows()<< "  "<< X.cols()<<endl;
    //cout<<X<<endl;
    
    //REMOVE MEAN FROM EACH VARIABLE
    //remove mean of each dimension, dimensions at each row
        
//    X.colwise() -= X.rowwise().sum()/X.cols(); //remove mean of each dimension
    X.rowwise() -= X.colwise().mean(); // remove mean of each image

//    cout<<"removed mean"<<endl<<X<<endl;

    //COMPUTE COVARIANCE MATRIX

    cout<<"before alloc"<<endl;
    MatrixXd S(X.rows(),X.rows());
    cout<<"after alloc"<<endl;
    S=X*X.transpose();
    
    #ifdef debug 
    cout<<"S"<<endl<<S<<endl;
    #endif  

//    namedWindow("ric",WINDOW_NORMAL);
//    imshow("ric",src);
//    waitKey();
    
    //EXTRACT BLOCKS
    //COV TENSOR UNFOLD MODE 1

    MatrixXd S1(src.rows,src.rows*src.cols*src.cols);
    for(int i=0;i<src.cols;i++){//I2
        S1.block(0,i*src.rows*src.cols,src.rows,src.rows*src.cols) = S.block(i*src.rows,0,src.rows,src.rows*src.cols);
    }
    
    cout<<"S1 "<<S1.rows()<< "  "<< S1.cols()<<endl;
    
    #ifdef debug 
    cout<<S1<<endl; 
    #endif  
    
    //COMPUTE U1
    BDCSVD < MatrixXd > svd(S1, Eigen::ComputeThinU | Eigen::ComputeThinV);
    MatrixXd U1;
    U1=svd.matrixU();
    
    
    //Multiply covariance tensor times U1^T and U2^T
    //S=G_S x1 U1 x2 U1 x3 U2 x4 U2
    
    //COMPUTE U1TxS 
    MatrixXd U1TxS;

    U1TxS=U1.transpose()*S1;
    
    #ifdef debug 
    cout<<"U1TxS"<<endl<<U1TxS<<endl;
    #endif 
    
    //ARRANGE U1TxS AS A TENSOR A1
    MatrixXd A1(U1.cols()*src.cols,src.rows*src.cols);//tensor of U1TxS
    
    for(int ii=0; ii<src.cols; ii++){
        A1.block(ii*U1.cols(),0,U1.cols(),src.rows*src.cols)=
        U1TxS.block(0,ii*src.rows*src.cols,U1.cols(),src.rows*src.cols);
    }
    
    #ifdef debug 
    cout<<"A1"<<endl<<A1<<endl;
    #endif 
    
//    Block operation	
//    Block of size (p,q), starting at (i,j)	
//    
//    Version constructing a dynamic-size block expression	
//    matrix.block(i,j,p,q);
//    
//    Version constructing a fixed-size block expression
//    matrix.block<p,q>(i,j);

    //COMPUTE A1 x3 U2^T
    
    //UNFOLD A1 in mode 3
    
    MatrixXd A1_3(src.cols, U1.cols()*src.rows*src.cols);
    

    for(int i2=0;i2<src.rows*src.cols;i2++){
        for(int i1=0;i1<U1.cols();i1++){//FILL I1=r1 columns
            for(int i3=0;i3<src.cols;i3++){ //FILL FIRST COLUMN
                A1_3(i3,i1+i2*U1.cols())=A1(i3*U1.cols()+i1,i2);
            }
        }
    }
    
    #ifdef debug 
    cout<<"A1_3 "<<endl<<A1_3<<endl;
    #endif 
    
    

    
    
    //******************************************************************
    //Extract rows and build S2
    //**************************************************************
    v=vector<VectorXd>(samples);
    
    for(int i=0;i<samples;i++){
        cv2eigen(srcvector[i].t(),eigaux[i]);
    //        cout<<"bgr"<<endl<<bgr[i]<<endl;
                
        v[i]=Map<VectorXd>(eigaux[i].data(), eigaux[i].size());
//        cout<<"v "<<v[i].rows()<<"   "<<v[i].cols()<<endl;
    }   

    X=MatrixXd(v[0].rows(), v.size());
    for(int i=0;i<v.size();i++){
        X.block(0,i,v[0].rows(),1) = v[i];
    }

    //    X.colwise() -= X.rowwise().sum()/X.cols(); //remove mean of each dimension
    X.rowwise() -= X.colwise().mean(); // remove mean of each image
    
    //    cout<<"X "<<X.rows()<< "  "<< X.cols()<<endl;

    //    cout<<"before alloc"<<endl;
    //    MatrixXd S(X.rows(),X.rows());
    //    cout<<"after alloc"<<endl;
    S=X*X.transpose();
    
    #ifdef debug
    cout<<"S"<<endl<<S<<endl;
    #endif
    
    //    imshow("ric",src);
    //    waitKey();
//  

    //EXTRACT BLOCKS AND UNFOLD INTO MODE 1

    MatrixXd S2(src.cols,src.rows*src.rows*src.cols);
  
    for(int i=0;i<src.rows;i++){//I1
        S2.block(0,i*src.rows*src.cols,src.cols,src.rows*src.cols)= S.block(i*src.cols,0,src.cols,src.rows*src.cols);
    }

    cout<<"S2 "<<S2.rows()<< "  "<< S2.cols()<<endl;
    #ifdef debug
    cout<<S2<<endl;
    #endif
    
    //compute U2
    BDCSVD < MatrixXd > svd2(S2, Eigen::ComputeThinU);
    MatrixXd U2;
    U2=svd2.matrixU();
    cout<<"DECOMPOSE S2"<<endl;
    //****************************************************
    //COMPUTE A1 x3 U2^T
    //*****************************************************
    
    MatrixXd A2;
    
    A2=U2.transpose()*A1_3;
    
    #ifdef debug
    cout<< "A2 "<<endl<<A2<<endl;
    #endif
    
    //**************************
    //EXTRACT IMAGE SPACES
    //**************************
    cout<<"beforeextractspaces"<<endl;
    vector<MatrixXd> spaces(U1.cols()*U2.cols());
    MatrixXd spaceaux(src.rows,src.cols);
    vector<VectorXd> vectoraux(U2.cols()*U1.cols(), VectorXd(src.rows*src.cols));
    MatrixXd joined(src.rows*src.cols,U2.cols()*U1.cols());
    Mat spacecv;
    Mat resulth;
    Mat resultv;
    
    namedWindow("ric",WINDOW_NORMAL);

    Mat filters;//imagespaces in rows
    
    for(int i4=0; i4<U2.cols();i4++){
        for(int i3=0;i3<U1.cols();i3++){
            for(int i1=0; i1<src.cols ; i1++){
                for(int i2=0; i2<src.rows; i2++){
                    spaceaux(i2,i1)=A2(i4,i3+i2*U1.cols()+i1*U1.cols()*src.rows);
                }
            }   
            
            #ifdef debug
            cout<< "spaceaux"<<endl<<spaceaux<<endl;
            #endif

            //imagespaces in eigen matrix spaceaux
            //vectorize each imagespace
//            cout<<"idx"<<i3+i4*U1.cols()<<"total"<<U2.cols()*U1.cols()<<endl;
            vectoraux[i3+i4*U1.cols()]=Map<VectorXd>(spaceaux.data(), spaceaux.size());
//            cout<<"termina"<<endl;
//            cout<<"v "<<vectoraux.rows()<<"  vectoraux "<<vectoraux.cols()<<endl;



            eigen2cv(spaceaux,spacecv);
                        
            double min, max;
            cv::minMaxLoc(spacecv, &min, &max);
           
            spacecv=(spacecv-min)/(max-min)*255;
            spacecv.convertTo(spacecv,0);
            
            if (resulth.empty()){
                spacecv.copyTo(resulth);
            }
            else{
                hconcat(resulth,spacecv,resulth);
            }
            
//            imshow("ric",spacecv);
//            waitKey();
        }
        
        if (resultv.empty()){
            resulth.copyTo(resultv);
        }
        else{
            vconcat(resultv,resulth,resultv);
        }
        resulth=Mat();
    }
    

    for(int i=0;i<vectoraux.size();i++){
//        cout<<"idx "<<i<<endl;
//        cout<<"joined "<<"rows "<<joined.rows()<<" cols "<<joined.cols()<<endl;
//        cout<<"vectoraux[0] "<<"rows "<<vectoraux[0].rows()<<" cols "<<vectoraux[0].cols()<<endl;
        joined.block(0,i,vectoraux[0].rows(),1) = vectoraux[i];

    }
    resize(resultv,resultv,Size(320,320));
    imshow("ric",resultv);
//    imwrite( "../pcanet/results/filter"+to_string(imageidx)+".bmp", resultv );
//    cout<<"../results/filter"+to_string(imageidx)+".bmp"<<endl;
//    waitKey();
   
    eigen2cv(joined,filters);

    return filters;
}


vector<Mat> im2matvect(cv::Mat& InImg, vector<int>& blockSize, vector<int>& stepSize){

    //int r_row = blockSize[ROW_DIM] * blockSize[COL_DIM];//7x7
    int row_diff = InImg.rows - blockSize[ROW_DIM];//32-7=25
    int col_diff = InImg.cols - blockSize[COL_DIM];//32-7=25
    size_t r_col = size_t( (row_diff / stepSize[ROW_DIM] + 1) * (col_diff / stepSize[COL_DIM] + 1) );
    //(25/1+1)*(25/1+1)=676
    vector<cv::Mat> OutBlocks(r_col, Mat(blockSize[0],blockSize[1], InImg.depth() ) );
    vector<cv::Mat> patches(r_col, Mat(blockSize[0],blockSize[1], InImg.depth() ) );

    double *p_InImg;//, *p_OutImg;
    size_t blocknum = 0;

    for(int j=0; j<=col_diff; j+=stepSize[COL_DIM]){
        for(int i=0; i<=row_diff; i+=stepSize[ROW_DIM]){

//            p_OutImg = OutBlocks[blocknum];

            for(int m=0; m<blockSize[ROW_DIM]; m++){

                p_InImg = InImg.ptr<double>(i + m);

                for(int l=0; l<blockSize[COL_DIM]; l++){
                    OutBlocks[blocknum].at<double>(m,l) = p_InImg[j + l];
                    //p_OutImg[blockSize[COL_DIM] * l + m] = p_InImg[j + l];
                }

            }
            patches[blocknum]=OutBlocks[blocknum].clone();

//show extracted patches
//            Mat auxblock, auximg;
//            resize(OutBlocks[blocknum],auxblock,Size(64,64));
//            resize(InImg,auximg,Size(256,256));
//            imshow("blocks",auxblock);
//            imshow("img",auximg);
//            waitKey();
            blocknum ++;
        }
    }

    return patches;
}



cv::Mat im2colstep(cv::Mat& InImg, vector<int>& blockSize, vector<int>& stepSize){

    int r_row = blockSize[ROW_DIM] * blockSize[COL_DIM];
    int row_diff = InImg.rows - blockSize[ROW_DIM];
    int col_diff = InImg.cols - blockSize[COL_DIM];
    int r_col = (row_diff / stepSize[ROW_DIM] + 1) * (col_diff / stepSize[COL_DIM] + 1);
    cv::Mat OutBlocks(r_col, r_row, InImg.depth());

    double *p_InImg, *p_OutImg;
    int blocknum = 0;

    for(int j=0; j<=col_diff; j+=stepSize[COL_DIM]){
        for(int i=0; i<=row_diff; i+=stepSize[ROW_DIM]){

            p_OutImg = OutBlocks.ptr<double>(blocknum);

            for(int m=0; m<blockSize[ROW_DIM]; m++){

                p_InImg = InImg.ptr<double>(i + m);

                for(int l=0; l<blockSize[COL_DIM]; l++){
                    p_OutImg[blockSize[ROW_DIM] * l + m] = p_InImg[j + l];
                    //p_OutImg[blockSize[COL_DIM] * l + m] = p_InImg[j + l];
                }

            }
            blocknum ++;
        }
    }

    return OutBlocks;
}

cv::Mat im2col_general(cv::Mat& InImg, vector<int>& blockSize, vector<int>& stepSize){
    assert(blockSize.size() == 2 && stepSize.size() == 2);

    int channels = InImg.channels();

    vector<cv::Mat> layers;
    if(channels > 1)
        split(InImg, layers);
    else
        layers.push_back(InImg.clone());

    cv::Mat AllBlocks = im2colstep(layers[0], blockSize, stepSize);

    size_t size = layers.size();
    if(size > 1){
        swap(layers[0], layers.back());
        layers.pop_back();
        for(size_t i=1; i<size; i++){
            hconcat(AllBlocks, im2colstep(layers[i], blockSize, stepSize), AllBlocks);
        }
    }
    return AllBlocks.t();
}

int* getRandom(int size){
    int* rand_idx = new int[size];
    int* idx = new int[size];
    for (int i=0; i<size; i++) {
        idx[i] = i;
    }
    int temp;
    int j=0;
    for(int i=size; i>0; i--){
        temp = rand() % i;
        rand_idx[j] = idx[temp];
        j++;
        idx[temp] = idx[i - 1];
    }
    return rand_idx;
}

cv::Mat PCA_FilterBank(vector<cv::Mat>& InImg, int PatchSize, int NumFilters){
    //for all the dataset, get patches, compute pca over patches dataset
    int channels = InImg[0].channels();
    size_t InImg_Size = InImg.size();

    int* randIdx = getRandom(int(InImg_Size));

    int size = channels * PatchSize * PatchSize;
    int img_depth = InImg[0].depth();
    cv::Mat Rx = cv::Mat::zeros(size, size, img_depth);

    vector<int> blockSize;
    vector<int> stepSize;

    for(int i=0; i<2; i++){
        blockSize.push_back(PatchSize);
        stepSize.push_back(1);
    }

    cv::Mat temp;
    cv::Mat mean;
    cv::Mat temp2;
    cv::Mat temp3;

    int coreNum = omp_get_num_procs();
    int cols = 0;

    vector<Mat> patches;
    MatrixXd sigma3=MatrixXd::Zero(676,676);
    bool tensor=false;
    MatrixXd M;
    cv::Mat Filters;

    vector<MatrixXd> covcumulated(2,MatrixXd::Zero(PatchSize*PatchSize,PatchSize*PatchSize));



//# pragma omp parallel for default(none) num_threads(coreNum) private(temp, temp2, temp3, mean) shared(cols, Rx, InImg_Size, InImg, randIdx, blockSize, stepSize)
    for(size_t j=0; j<InImg_Size; j++){//InImg_Size for each image

        if(tensor==false){
            temp = im2col_general(InImg[randIdx[int(j)]], blockSize, stepSize);
            //temp = im2col_general(InImg[j], blockSize, stepSize);
            //temp has all the patches, arranged as columns

            cv::reduce(temp, mean, 0, CV_REDUCE_AVG);//compute mean of each sample
            //row vector with as much columns as samples
            temp3.create(0, temp.cols, temp.type());
            cols = temp.cols;
            for(int i=0;i<temp.rows;i++){
                temp2 = (temp.row(i) - mean.row(0));//remove mean of each sample
                temp3.push_back(temp2.row(0));
            }

            //ric edit
            //show patches, remove #pragma
            //        imshow("riccomplete",InImg[j]);
            //        cv::Mat corrblock;

            //        for (int ii=0;ii<temp.cols;ii++) {
            //            //cv::circle(InImg[randIdx[j]], cv::Point2f(floor(ii/InImg[randIdx[j]].cols)ii%InImg[randIdx[j]].rows), 1, cv::Scalar(255));
            //            temp.col(ii).copyTo(corrblock);
            //            corrblock.convertTo(corrblock,CV_8UC1,600);
            //            cv::Mat transposed;
            //            transposed=corrblock.t();
            //            cv::imshow("ric",transposed.reshape(1,7).t());
            //            cv::waitKey(1000);
            //        }
            //end

            temp = temp3 * temp3.t();
    //# pragma omp critical
            Rx = Rx + temp;
        }
        else {
            //objective: cumulate covariannce

            patches = im2matvect(InImg[randIdx[int(j)]], blockSize, stepSize);
            cols =patches.size();
            Mat tfilters;
//            for(int ii=0;ii<patches.size();ii++){
//                Mat aux=patches[ii];
//                resize(aux,aux,Size(),4,4);
//                imshow("ric",aux);
//                waitKey();
//            }

            vector<MatrixXd> covariancevector;
            covariancevector=computecovariance(patches);

            covcumulated[0]=covcumulated[0]+covariancevector[0];
            covcumulated[1]=covcumulated[1]+covariancevector[1];


//            cout<<"working"<<endl;

//            tfilters=tensorfilters(patches,randIdx[int(j)]);


        }//if vector or tensor

    }//for each image

    if(tensor==false){
        Rx = Rx / double(InImg_Size * cols);

        cv::Mat eValuesMat;
        cv::Mat eVectorsMat;

        eigen(Rx, eValuesMat, eVectorsMat);

        cout<<"eVectorsMat"<<eVectorsMat.size<<endl;

        Filters=Mat(0, Rx.cols, Rx.depth());

        for(int i=0; i<Rx.rows; i++){//NumFilters
            Filters.push_back(eVectorsMat.row(i));//row

            //        //ric edit
            //show filters
            //        cv::Mat corrblock;
            //        eVectorsMat.row(i).copyTo(corrblock);
            //        corrblock.convertTo(corrblock,CV_8UC1,600);
            //        cv::imshow("ric",corrblock.reshape(1,7));
            //        cv::waitKey(1000);
            //        //end
        }
    }
    else {

        covcumulated[0]=covcumulated[0]/ double(InImg_Size * cols);
        covcumulated[1]=covcumulated[1]/ double(InImg_Size * cols);
        Filters=filtfromcov(covcumulated, Mat(PatchSize,PatchSize,1));//transpose to get the correct filters .t()

    }

    return Filters;
}	

PCA_Out_Result* PCA_output(vector<cv::Mat>& InImg, vector<int>& InImgIdx, int PatchSize, int NumFilters, cv::Mat& Filters, int threadnum){
    //compute the correlation between patches and filters
    PCA_Out_Result* result = new PCA_Out_Result;

    int img_length = InImg.size();
    int mag = (PatchSize - 1) / 2;
    int channels = InImg[0].channels();


    cv::Mat img;

    vector<int> blockSize;
    vector<int> stepSize;

    for(int i=0; i<2; i++){
        blockSize.push_back(PatchSize);
        stepSize.push_back(1);
    }

    cv::Mat temp;
    cv::Mat mean;
    cv::Mat temp2;
    cv::Mat temp3;

    int coreNum = omp_get_num_procs();
    coreNum = coreNum > threadnum ? threadnum : coreNum;
    cv::Scalar s = cv::Scalar(0);

# pragma omp parallel for default(none) num_threads(coreNum) private(img, temp, temp2, temp3, mean) shared(InImgIdx, s, blockSize, stepSize, mag, img_length, InImg, result, Filters, NumFilters)
    for(int i=0; i<img_length; i++){

        cv::copyMakeBorder(InImg[i], img, mag, mag, mag, mag, cv::BORDER_CONSTANT, s);

        temp = im2col_general(img, blockSize, stepSize);

        cv::reduce(temp, mean, 0, CV_REDUCE_AVG);

        temp3.create(0, temp.cols, temp.type());

        for(int i=0;i<temp.rows;i++){
            temp2 = (temp.row(i) - mean.row(0));
            temp3.push_back(temp2.row(0));
        }
# pragma omp critical
        {
            result->OutImgIdx.push_back(InImgIdx[i]);
            for(int j=0; j<NumFilters; j++){
                temp = Filters.row(j) * temp3;
                temp = temp.reshape(0, InImg[i].cols);
                result->OutImg.push_back(temp.clone().t());
            }
        }
    }

    //**************ric edit
    //*********show filtered images
    //    cv::Mat corrblock;

    //    for (int ii=0;ii<result->OutImg.size();ii++) {
    //        //        //cv::circle(InImg[randIdx[j]], cv::Point2f(floor(ii/InImg[randIdx[j]].cols)ii%InImg[randIdx[j]].rows), 1, cv::Scalar(255));
    //        //        temp.col(ii).copyTo(corrblock);
    //        //        cv::Mat transposed;
    //        //        transposed=corrblock.t();

    //        cv::Mat myfilter=Filters.row(ii%8).t();
    //        std::cout<<myfilter<<endl;

    //        double min, max;
    //        cv::minMaxLoc(myfilter, &min, &max);
    //        myfilter=myfilter-min;
    //        cv::minMaxLoc(myfilter, &min, &max);
    //        myfilter=myfilter/max;

    //        cv::Mat filtconverted;
    //        myfilter.convertTo(filtconverted,CV_8UC1,255);
    //        cv::Mat im_color;
    //        cv::applyColorMap(filtconverted.reshape(1,7), im_color, 4);
    //        cv::imshow("ricfilter",im_color);
    //        cv::imshow("ric",result->OutImg[ii]);
    //        cv::waitKey(1000);
    //    }
    //******************end

    return result;
}

PCA_Train_Result* PCANet_train(vector<cv::Mat>& InImg, PCANet* PcaNet, bool is_extract_feature){
    assert(PcaNet->NumFilters.size() == PcaNet->NumStages);

    PCA_Train_Result* train_result = new PCA_Train_Result;
    PCA_Out_Result* out_result = new PCA_Out_Result;
    PCA_Out_Result* temp;


    //    for(int ii=0;ii<InImg.size();ii++){
    //        cv::imshow("input", InImg[ii]);
    //        cv::waitKey(100);
    //    }

    out_result->OutImg = InImg;
    int img_length = InImg.size();
    for(int i=0; i<img_length; i++)
        out_result->OutImgIdx.push_back(i);

    int64 e1 = cv::getTickCount();
    int64 eo1, eo2, eb1, eb2;

    for(int s=0; s<PcaNet->NumStages; s++){//for each stage
        eb1 = cv::getTickCount();
        cout << " Computing PCA filter bank and its outputs at stage " << s << "..." << endl;
        train_result->Filters.push_back(PCA_FilterBank(out_result->OutImg, PcaNet->PatchSize, PcaNet->NumFilters[s]).clone());
        eb2 = cv::getTickCount();
        cout <<" stage"<<s<<" PCA_FilterBank time: "<<(eb2 - eb1)/ cv::getTickFrequency()<<endl;


        //ric
        //save filters

//        cv::Mat corrblock;
//        cout<<"train_result->Filters[s].rows"<<train_result->Filters[s].rows<<endl;
//        for (int ii=0;ii<train_result->Filters[s].rows;ii++) {
//            train_result->Filters[s].row(ii).copyTo(corrblock);
//            cout<<"channels"<<corrblock.depth()<<endl;
//            cout<<"corrblock"<<corrblock.at<double>(0.0)<<endl;

//            double min, max;
//            cv::minMaxLoc(corrblock, &min, &max);

//            corrblock=(corrblock-min)/(max-min)*255;
//            corrblock.convertTo(corrblock,0);

////            cv::imshow("ric",corrblock.reshape(1,7));
//            cv::Mat dst;
//            cv::resize(corrblock.reshape(1,7),dst,cv::Size(100,100));//reshape transpose image
//            cv::imshow("corrblock",dst);

////            cv::imwrite( "filter"+to_string(ii)+"layer"+to_string(s)+".jpg", dst);
//            cv::waitKey();
//        }


        //*****end


        eo1 = cv::getTickCount();
        if(s != PcaNet->NumStages - 1){
            temp = PCA_output(out_result->OutImg, out_result->OutImgIdx, PcaNet->PatchSize, PcaNet->NumFilters[s], train_result->Filters[s], omp_get_num_procs());
            delete out_result;
            out_result = temp;
        }
        eo2 = cv::getTickCount();
        cout <<" stage"<<s<<" output time: "<<(eo2 - eo1)/ cv::getTickFrequency()<<endl;
    }
    int64 e2 = cv::getTickCount();
    double time = (e2 - e1)/ cv::getTickFrequency();
    cout <<"\n total FilterBank time: "<<time<<endl;

    InImg.clear();
    vector<cv::Mat>().swap(InImg);

    vector<cv::Mat> tempF;
    int end = PcaNet->NumStages - 1;
    int outIdx_length = out_result->OutImgIdx.size();

    if(is_extract_feature){

        vector<cv::Mat>::const_iterator first = out_result->OutImg.begin();
        vector<cv::Mat>::const_iterator last = out_result->OutImg.begin();

        vector<cv::Mat> features;
        Hashing_Result* hashing_r;

        int coreNum = omp_get_num_procs();
        e1 = cv::getTickCount();
# pragma omp parallel for default(none) num_threads(coreNum) private(temp, hashing_r) shared(features, out_result, PcaNet, first, last, outIdx_length, img_length, train_result, end)
        for(int i=0; i<img_length; i++){
            vector<cv::Mat> subInImg(first + i * PcaNet->NumFilters[end], last + (i + 1) * PcaNet->NumFilters[end]);
            vector<int> subIdx;
            for(int j=0; j< PcaNet->NumFilters[end]; j++)
                subIdx.push_back(j);

            temp = PCA_output(subInImg, subIdx, PcaNet->PatchSize,
                              PcaNet->NumFilters[end], train_result->Filters[end], 2);

            hashing_r = HashingHist(PcaNet, temp->OutImgIdx, temp->OutImg);

#pragma omp critical
            {
                features.push_back(hashing_r->Features);

//                cv::Mat ricfeatures=hashing_r->Features;
//                ricfeatures=ricfeatures(cv::Rect2d(0,0,400,1));
//                resize(ricfeatures,ricfeatures,cv::Size(),4,50);
//                cv::imshow("ricfeat",ricfeatures);
//                cv::imshow("input",InImg[i]);
//                cv::waitKey(100);
//                cout<<"imgidx "<<i<<endl;

                train_result->feature_idx.push_back(out_result->OutImgIdx[i]);
            }
            delete hashing_r;
            delete temp;
            subIdx.clear();
            vector<int>().swap(subIdx);
        }
        e2 = cv::getTickCount();
        time = (e2 - e1)/ cv::getTickFrequency();
        cout <<"\n hasing time: "<<time<<endl;

        delete out_result;

        int size = features.size();
        if(size > 0){

            train_result->Features.create(0, features[0].cols, features[0].type());
            for(int i=0 ;i<size; i++){
                train_result->Features.push_back(features[i]);
            }
        }

        features.clear();
        vector<cv::Mat>().swap(features);
    }
    return train_result;
}

double round(double r){  
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}  

Hashing_Result* HashingHist(PCANet* PcaNet, vector<int>& ImgIdx, vector<cv::Mat>& Imgs){
    Hashing_Result* ha_result = new Hashing_Result;

    int length = Imgs.size();
    int NumFilters =  PcaNet->NumFilters[PcaNet->NumStages - 1];
    int NumImgin0 = length / NumFilters;

    cv::Mat T;
    int row = Imgs[0].rows;
    int col = Imgs[0].cols;
    int depth = Imgs[0].depth();

    vector<double> map_weights;
    cv::Mat temp;
    for(int i=NumFilters - 1; i>=0; i--)
        map_weights.push_back(pow(2.0, (double)i));

    vector<int> Ro_BlockSize;
    double rate = 1 - PcaNet->BlkOverLapRatio;
    for(int i=0 ;i<PcaNet->HistBlockSize.size(); i++) {
        Ro_BlockSize.push_back(round(PcaNet->HistBlockSize[i] * rate));
    }


    cv::Mat BHist;

    int ImgIdx_length = ImgIdx.size();
    int new_idx[ImgIdx_length]; //= new int[ImgIdx_length];
    for(int i=0; i<ImgIdx_length; i++)
        new_idx[ImgIdx[i]] = i;

    for(int i=0; i<NumImgin0; i++){
        T = cv::Mat::zeros(row, col, depth);

        for(int j=0; j<NumFilters; j++){
            temp = Heaviside(Imgs[NumFilters * new_idx[i] + j]);
            temp = temp * map_weights[j];
            T = T + temp;
        }

        temp = im2col_general(T, PcaNet->HistBlockSize, Ro_BlockSize);
        temp = Hist(temp, (int)(pow(2.0, NumFilters)) - 1);

        temp = bsxfun_times(temp, NumFilters);

        if(i == 0) BHist = temp;
        else hconcat(BHist, temp, BHist);
    }

    int rows = BHist.rows;
    int cols = BHist.cols;

    ha_result->Features.create(1, rows * cols, BHist.type());

    double *p_Fe = ha_result->Features.ptr<double>(0);
    double *p_Hi;
    for(int i=0; i<rows; i++){
        p_Hi = BHist.ptr<double>(i);
        for(int j=0; j<cols; j++){
            p_Fe[j * rows + i] = p_Hi[j];
        }
    }
    return ha_result;
}


cv::Mat Heaviside(cv::Mat& X){
    int row = X.rows;
    int col = X.cols;
    int depth = X.depth();

    cv::Mat H(row, col, depth);

    double *p_X, *p_H;

    for(int i=0; i<row; i++){
        p_X = X.ptr<double>(i);
        p_H = H.ptr<double>(i);

        for(int j=0; j<col; j++){
            if(p_X[j] > 0) p_H[j] = 1;
            else p_H[j] = 0;
        }
    }
    return H;
}

cv::Mat Hist(cv::Mat& mat, int Range){
    cv::Mat temp = mat.t();
    int row = temp.rows;
    int col = temp.cols;
    int depth = temp.depth();
    cv::Mat Hist = cv::Mat::zeros(row, Range + 1, depth);

    double *p_M, *p_H;

    for(int i=0; i<row; i++){
        p_M = temp.ptr<double>(i);
        p_H = Hist.ptr<double>(i);

        for(int j=0; j<col; j++){
            p_H[(int)p_M[j]] += 1;
        }
    }

    temp = Hist.t();

    return temp;
}


cv::Mat bsxfun_times(cv::Mat& BHist, int NumFilters){

    double *p_BHist;
    int row = BHist.rows;
    int col = BHist.cols;

    //double* sum = new double[col];
    double sum[col];
    for(int i=0; i<col; i++)
        sum[i] = 0;

    for(int i=0; i<row; i++){
        p_BHist = BHist.ptr<double>(i);
        for(int j=0; j<col; j++)
            sum[j] += p_BHist[j];
    }
    double p = pow(2.0, NumFilters);

    for(int i=0; i<col; i++)
        sum[i] = p / sum[i];

    for(int i=0; i<row; i++){
        p_BHist = BHist.ptr<double>(i);
        for(int j=0; j<col; j++)
            p_BHist[j] *= sum[j];
    }

    return BHist;
}
