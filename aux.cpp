

vector<MatrixXd> computecovariance(vector<Mat> srcvector, int imageidx)
{

    Mat src= srcvector[0];
    int64 e1,e2;
    double time;
 
    //CONVERT FROM OCV TO EIGEN
    //vector of mats to eigen
    //tensor from image
    int samples=srcvector.size();
    vector<MatrixXd> eigaux(samples);//(src.rows, src.cols)
    vector<VectorXd> v(samples);

    //UNFOLD MATRICES TO COLUMN VECTORS
    for(int i=0;i<samples;i++){
        cv2eigen(srcvector[i],eigaux[i]);
        
        //cout<<"bgr"<<endl<<bgr[i]<<endl;
        
        //Vectorize matrices
        v[i]=Map<VectorXd>(eigaux[i].data(), eigaux[i].size());
//        cout<<endl << "v[i].rows() "<<v[i].rows()<<" v[i].cols()  "<<v[i].cols()<<endl;
        //cout<<v[i]<<endl;
    }   
    
    
    //ARRANGE VECTORIZED IMAGES TO MATRIX X
    MatrixXd X(v[0].rows(), v.size());

    for(int i=0;i<v.size();i++){
        X.block(0,i,v[0].rows(),1) = v[i];
    }
    
    cout<<"X "<<X.rows()<< "  "<< X.cols()<<endl;
    //cout<<X<<endl;
    
    //REMOVE MEAN FROM EACH VARIABLE
    //remove mean of each dimension, dimensions at each row
        
//    X.colwise() -= X.rowwise().sum()/X.cols(); //remove mean of each dimension
    X.rowwise() -= X.colwise().mean(); // remove mean of each image

//    cout<<"removed mean"<<endl<<X<<endl;

    //COMPUTE COVARIANCE MATRIX

    cout<<"before alloc"<<endl;
    MatrixXd S1(X.rows(),X.rows());
    cout<<"after alloc"<<endl;
    S1=X*X.transpose();
    
    
    
    //******************************************************************
    //Extract rows and build S2
    //**************************************************************
    v=vector<VectorXd>(samples);
    
    for(int i=0;i<samples;i++){
        cv2eigen(srcvector[i].t(),eigaux[i]);
    //        cout<<"bgr"<<endl<<bgr[i]<<endl;
                
        v[i]=Map<VectorXd>(eigaux[i].data(), eigaux[i].size());
//        cout<<"v "<<v[i].rows()<<"   "<<v[i].cols()<<endl;
    }   

    X=MatrixXd(v[0].rows(), v.size());
    for(int i=0;i<v.size();i++){
        X.block(0,i,v[0].rows(),1) = v[i];
    }

    //    X.colwise() -= X.rowwise().sum()/X.cols(); //remove mean of each dimension
    X.rowwise() -= X.colwise().mean(); // remove mean of each image
    
    //    cout<<"X "<<X.rows()<< "  "<< X.cols()<<endl;

    //    cout<<"before alloc"<<endl;
    MatrixXd S2(X.rows(),X.rows());
    //    cout<<"after alloc"<<endl;
    S2=X*X.transpose();
 
    vector<MatrixXd> covariancevector;
    covariancevector.push_back(S1);
    covariancevector.push_back(S2);
    
    
    return covariancevector;      
}


Mat tensorfilters(vector<MatrixXd> S, Mat src)
{
    //EXTRACT BLOCKS
    //COV TENSOR UNFOLD MODE 1

    MatrixXd S1(src.rows,src.rows*src.cols*src.cols);
    for(int i=0;i<src.cols;i++){//I2
        S1.block(0,i*src.rows*src.cols,src.rows,src.rows*src.cols) = S[0].block(i*src.rows,0,src.rows,src.rows*src.cols);
    }
    
//    cout<<"S1 "<<S1.rows()<< "  "<< S1.cols()<<endl;
    
//    #ifdef debug 
//    cout<<S1<<endl; 
//    #endif  
    
    //COMPUTE U1
    BDCSVD < MatrixXd > svd(S1, Eigen::ComputeThinU | Eigen::ComputeThinV);
    MatrixXd U1;
    U1=svd.matrixU();
    
    
    //Multiply covariance tensor times U1^T and U2^T
    //S=G_S x1 U1 x2 U1 x3 U2 x4 U2
    
    //COMPUTE U1TxS 
    MatrixXd U1TxS;

    U1TxS=U1.transpose()*S1;
    
    #ifdef debug 
    cout<<"U1TxS"<<endl<<U1TxS<<endl;
    #endif 
    
    //ARRANGE U1TxS AS A TENSOR A1
    MatrixXd A1(U1.cols()*src.cols,src.rows*src.cols);//tensor of U1TxS
    
    for(int ii=0; ii<src.cols; ii++){
        A1.block(ii*U1.cols(),0,U1.cols(),src.rows*src.cols)=
        U1TxS.block(0,ii*src.rows*src.cols,U1.cols(),src.rows*src.cols);
    }
    
    #ifdef debug 
    cout<<"A1"<<endl<<A1<<endl;
    #endif 
    
//    Block operation	
//    Block of size (p,q), starting at (i,j)	
//    
//    Version constructing a dynamic-size block expression	
//    matrix.block(i,j,p,q);
//    
//    Version constructing a fixed-size block expression
//    matrix.block<p,q>(i,j);

    //COMPUTE A1 x3 U2^T
    
    //UNFOLD A1 in mode 3
    
    MatrixXd A1_3(src.cols, U1.cols()*src.rows*src.cols);
    

    for(int i2=0;i2<src.rows*src.cols;i2++){
        for(int i1=0;i1<U1.cols();i1++){//FILL I1=r1 columns
            for(int i3=0;i3<src.cols;i3++){ //FILL FIRST COLUMN
                A1_3(i3,i1+i2*U1.cols())=A1(i3*U1.cols()+i1,i2);
            }
        }
    }
    
    #ifdef debug 
    cout<<"A1_3 "<<endl<<A1_3<<endl;
    #endif 
    
    //***********************************************************
    //MODE 3 FACTOR MATRIX
    
    //EXTRACT BLOCKS AND UNFOLD INTO MODE 1

    MatrixXd S2(src.cols,src.rows*src.rows*src.cols);
  
    for(int i=0;i<src.rows;i++){//I1
        S2.block(0,i*src.rows*src.cols,src.cols,src.rows*src.cols)= S[1].block(i*src.cols,0,src.cols,src.rows*src.cols);
    }

//    cout<<"S2 "<<S2.rows()<< "  "<< S2.cols()<<endl;
//    #ifdef debug
//    cout<<S2<<endl;
//    #endif
    
    //compute U2
    BDCSVD < MatrixXd > svd2(S2, Eigen::ComputeThinU);
    MatrixXd U2;
    U2=svd2.matrixU();
    cout<<"DECOMPOSE S2"<<endl;
    //****************************************************
    //COMPUTE A1 x3 U2^T
    //*****************************************************
    
    MatrixXd A2;
    
    A2=U2.transpose()*A1_3;
    
    #ifdef debug
    cout<< "A2 "<<endl<<A2<<endl;
    #endif
    
    //**************************
    //EXTRACT IMAGE SPACES
    //**************************
    cout<<"beforeextractspaces"<<endl;
    vector<MatrixXd> spaces(U1.cols()*U2.cols());
    MatrixXd spaceaux(src.rows,src.cols);
    vector<VectorXd> vectoraux(U2.cols()*U1.cols(), VectorXd(src.rows*src.cols));
    MatrixXd joined(src.rows*src.cols,U2.cols()*U1.cols());
    Mat spacecv;
    Mat resulth;
    Mat resultv;
    
    namedWindow("ric",WINDOW_NORMAL);

    Mat filters;//imagespaces in rows
    
    for(int i4=0; i4<U2.cols();i4++){
        for(int i3=0;i3<U1.cols();i3++){
            for(int i1=0; i1<src.cols ; i1++){
                for(int i2=0; i2<src.rows; i2++){
                    spaceaux(i2,i1)=A2(i4,i3+i2*U1.cols()+i1*U1.cols()*src.rows);
                }
            }   
            
            #ifdef debug
            cout<< "spaceaux"<<endl<<spaceaux<<endl;
            #endif

            //imagespaces in eigen matrix spaceaux
            //vectorize each imagespace
//            cout<<"idx"<<i3+i4*U1.cols()<<"total"<<U2.cols()*U1.cols()<<endl;
            vectoraux[i3+i4*U1.cols()]=Map<VectorXd>(spaceaux.data(), spaceaux.size());
//            cout<<"termina"<<endl;
//            cout<<"v "<<vectoraux.rows()<<"  vectoraux "<<vectoraux.cols()<<endl;



            eigen2cv(spaceaux,spacecv);
                        
            double min, max;
            cv::minMaxLoc(spacecv, &min, &max);
           
            spacecv=(spacecv-min)/(max-min)*255;
            spacecv.convertTo(spacecv,0);
            
            if (resulth.empty()){
                spacecv.copyTo(resulth);
            }
            else{
                hconcat(resulth,spacecv,resulth);
            }
            
//            imshow("ric",spacecv);
//            waitKey();
        }
        
        if (resultv.empty()){
            resulth.copyTo(resultv);
        }
        else{
            vconcat(resultv,resulth,resultv);
        }
        resulth=Mat();
    }
    

    for(int i=0;i<vectoraux.size();i++){
//        cout<<"idx "<<i<<endl;
//        cout<<"joined "<<"rows "<<joined.rows()<<" cols "<<joined.cols()<<endl;
//        cout<<"vectoraux[0] "<<"rows "<<vectoraux[0].rows()<<" cols "<<vectoraux[0].cols()<<endl;
        joined.block(0,i,vectoraux[0].rows(),1) = vectoraux[i];

    }
    resize(resultv,resultv,Size(320,320));
    imshow("ric",resultv);
//    imwrite( "../pcanet/results/filter"+to_string(imageidx)+".bmp", resultv );
//    cout<<"../results/filter"+to_string(imageidx)+".bmp"<<endl;
    waitKey();
   
    eigen2cv(joined,filters);

    return filters;
    
}



//            //1 BUILD TENSOR AS A MODE1 UNFOLDED MATRIX
//            //tensor from image

//            MatrixXd X(patches[0].rows, patches[0].cols*patches.size());

//            Mat mode1=patches[0].clone();
//            for(int ii=1;ii<patches.size();ii++){
////                cout<<"mat"<<ii<<endl;
//                hconcat(mode1,patches[ii],mode1);
//            }

//            cv2eigen(mode1,X);
//            std::cout << "The matrix X is of size "<< X.rows() << "x" << X.cols() << std::endl;
//            std::cout << "It has " << X.size() << " coefficients" <<    std::endl;

//            //2 UNFOLD TENSOR TO MODE 3

//            vector<int> s{patches[0].rows,patches[0].cols, int(patches.size() ) };
//            vector<int> sprod{ 1,s[0],s[0]*s[1],s[0]*s[1]*s[2] };

//            int n=3;

//            for (uint8_t dim = 2; dim < 3; ++dim) {
//                cout << "\tUnfold (" << dim+1 << ")... " << endl;
//                M = MatrixXd(s[dim], sprod[n]/s[dim]); // dim-th factor matrix
//                #pragma omp parallel for
//                for (int64_t j = 0; j < X.cols(); ++j) {
//                    uint32_t write_i = (j/sprod[dim-1]) % s[dim];
//                    size_t base_write_j = j%sprod[dim-1] + j/(sprod[dim-1]*s[dim])*sprod[dim];
//                    for (int32_t i = 0; i < X.rows(); ++i)
//                        M(write_i, base_write_j + i*sprod[dim-1]) = X(i, j);
//                }

//                cout<<"unfold size "<<M.rows()<< " x "<< M.cols()<<endl;

//            }
//            //unfolding end

            //3 CUMULATIVE COVARIANCE
            //remove mean of each dimension, dimensions at each row

//            M.colwise() -= M.rowwise().sum()/M.cols();

//            sigma3=sigma3+M * M.transpose();
//            cout<<"covariance**************************"<<endl;

//*********************************************************************************************************

//*********************************************************************************************************

//*********************************************************************************************************



//        //4 Decompose

//        MatrixXd U3;
//        SelfAdjointEigenSolver < MatrixXd > decomp(sigma3); // M*M^T is symmetric -> faster eigenvalue computation

//        VectorXd eigenvalues = decomp.eigenvalues().real();
//        U3 = decomp.eigenvectors().real();

//        //5 project
//        MatrixXd M_proj;

//        M_proj = M.transpose() * U3;

//        cout<<"U3 times X size "<< M_proj.rows() << "x" << M_proj.cols()<<endl;

//        //6 Extract filters

//        //from mode3 M_proj get filters in core

//        vector<int> s{PatchSize,PatchSize, int(M_proj.cols())};//size of tensor patches
//        vector<int> sprod{ 1,s[0],s[0]*s[1],s[0]*s[1]*s[2] };

//        int n=3;
//        vector<MatrixXd> core( size_t(M_proj.cols()) ,MatrixXd(PatchSize,PatchSize));//512*3
//        MatrixXd coreaux(PatchSize,PatchSize);

//        for (uint32_t i = 0; i < s[n-1]; i++){//i<s[n-1]
//            for (size_t j = 0; j < sprod[n-1]; j++){
//                coreaux(j%PatchSize,(int) floor(j/PatchSize)) = M_proj(j,i);
//            }
//            core[(int)i]=coreaux;
//        }

//        for(int ii=0; ii<core.size();ii++){
//            Mat corecv;
//            eigen2cv(core[ii],corecv);
//            resize(corecv,corecv,Size(280,280));

//            Mat coreint1;

//            double min1,max1;
//            cv::minMaxLoc(corecv, &min1, &max1);
////            cout<<"minval"<<min1<<"maxval"<<max1<<endl;
//            corecv=(corecv-min1)/(max1-min1)*255;

//            corecv.convertTo(coreint1,0);

//            cout<<ii<<endl;
//            imshow("core",coreint1);
//            waitKey(0);

//        }




